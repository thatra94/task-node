const http = require('http');
const fs = require('fs');
const os = require('os');

const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});

const  options = `
Choose an option:
1. Read package.json
2. Display OS info
3. Start HTTP server
Type a number: `


readline.question(options, optionNumber => {

        switch (parseInt(optionNumber)) {
            case 1:
                readJsonFile()
                break
            case 2:
                displayOsInfo()
                break
            case 3:
                startServer()
                break
            default:
                console.log('Invalid option.')
        }
    readline.close();
}); 

function readJsonFile() {
    console.log('Reading package.json file')
    fs.readFile(__dirname + '/package.json', 'utf-8', (err, content) => {
        console.log(content);
    });
}

function displayOsInfo() {
    console.log('Getting OS info...')
    console.log(`
    SYSTEM MEMORY ${(os.totalmem() / 1024 / 1024 / 1024).toFixed(2)} GB
    FREE MEMORY: ${(os.freemem() / 1024 / 1024 / 1024).toFixed(2)} GB
    CPU CORES: ${(os.cpus().length)}
    ARCH: ${(os.arch())} 
    PLATFORM: ${(os.platform())}
    RELEASE: ${(os.release())} 
    USER: ${(os.userInfo().username)}`)
}


function startServer() {
    console.log('Starting HTTP server...')
    server.listen(3000);
    console.log('Listening on port 3000...')
}

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello, World!\n');
});


